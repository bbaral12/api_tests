package apiEngine.model.requests;

import java.util.ArrayList;
import java.util.List;

public class AddBooksRequest {
    public String userId;
    public List<ISBN> collectionOfIsbns;

    public AddBooksRequest(String userID, ISBN isbn){
        this.userId = userID;
        collectionOfIsbns = new ArrayList<ISBN>();
        collectionOfIsbns.add(isbn);

    }

}
