package apiEngine.model.requests;

public class RemoveBookRequest {
    public String userId;
    public String isbn;

    public RemoveBookRequest(String userID, String bookISBN){
        this.userId = userID;
        this.isbn = bookISBN;
    }
}
