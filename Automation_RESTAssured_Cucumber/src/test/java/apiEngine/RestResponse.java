package apiEngine;

import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.Arrays;

public class RestResponse<T> implements IRestResponse<T>{
    private T data;
    private Response response;
    private Exception e;

    public RestResponse(Class<T> t, Response response){
        this.response = response;
        try{
            this.data = t.getDeclaredConstructor().newInstance();
        }catch (Exception e){
            throw new RuntimeException("Default construct required in the Response POJO");
        }
    }

    @Override
    public T getBody() {
        try {
            data = (T) response.getBody().as(data.getClass());
        }catch (Exception e) {
            this.e=e;
        }
        return data;
    }

    @Override
    public String getContent() {
        return response.getBody().asString();
    }

    @Override
    public int getStatusCode() {
        return response.getStatusCode();
    }

    @Override
    public boolean isSuccessful() {
        ArrayList<Integer> validCodes = new ArrayList<>(Arrays.asList(200, 201, 202, 203, 204, 205));
        return validCodes.contains(response.getStatusCode());
    }

    @Override
    public String getStatusDescription() {
        return response.getStatusLine();
    }

    @Override
    public Response getResponse() {
        return response;
    }

    @Override
    public Exception getException() {
        return e;
    }
}
