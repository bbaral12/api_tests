package apiEngine;

import apiEngine.model.requests.AddBooksRequest;
import apiEngine.model.requests.AuthorizationRequest;
import apiEngine.model.requests.RemoveBookRequest;
import apiEngine.model.responses.Books;
import apiEngine.model.responses.TokenResponse;
import apiEngine.model.responses.UserAccount;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class EndPoints {
    private final RequestSpecification request;

    public EndPoints(String baseURL){
        RestAssured.baseURI = baseURL;
        request = RestAssured.given();
        request.header("Content-Type", "application/json");
    }

    public void authenticateUser(AuthorizationRequest authRequest){
        Response response = request.body(authRequest).post(Route.generateToken());
        TokenResponse tokenResponse = response.body().jsonPath().getObject("$", TokenResponse.class);

        if (tokenResponse.token == null){
            throw new RuntimeException("Authentication Failed. Result of failed Response: "+ tokenResponse.result);
        }

        request.header("Authorization", "Bearer "+tokenResponse.token);
    }

    public IRestResponse<Books> getBooks(){
        Response response = request.get(Route.books());
        return new RestResponse(Books.class, response);
    }

    public Response addBook(AddBooksRequest addBooksRequest){
        Response response = request.body(addBooksRequest).post(Route.books());
        return response;
    }

    public Response removeBook(RemoveBookRequest removeBookRequest){
        Response response = request.body(removeBookRequest).delete(Route.book());
        return response;
    }

    public IRestResponse<UserAccount> getUserAccount(String userID){
        Response response = request.get(Route.userAccount(userID));
        return new RestResponse(UserAccount.class, response);
    }

}
