package stepDefinitions;

import apiEngine.IRestResponse;
import apiEngine.model.Book;
import apiEngine.model.requests.AddBooksRequest;
import apiEngine.model.requests.ISBN;
import apiEngine.model.requests.RemoveBookRequest;
import apiEngine.model.responses.Books;
import cucumber.TestContext;
import enums.Context;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

public class BooksSteps extends BaseStep{

    private static Response response;

    public BooksSteps(TestContext testContext){
        super(testContext);
    }

    @Given("A list of books are available")
    public void getListOfBooksAvailable() {
        IRestResponse<Books> bookResponse = getEndPoints().getBooks();
        Assertions.assertTrue(bookResponse.isSuccessful());
        Book book = bookResponse.getBody().books.get(0);
        getScenarioContext().setContext(Context.BOOK, book);
    }

    @When("I add a book to my reading list")
    public void checkoutBookFromLibrary() {
        Book book = (Book) getScenarioContext().getContext(Context.BOOK);
        String userId = (String) getScenarioContext().getContext(Context.USER_ID);
        AddBooksRequest addBooksRequest = new AddBooksRequest(userId, new ISBN(book.isbn));
        response = getEndPoints().addBook(addBooksRequest);
        Assertions.assertEquals(201, response.getStatusCode());
    }

    @When("I remove the book from my reading list")
    public void returnBookToLibrary() {
        Book book = (Book) getScenarioContext().getContext(Context.BOOK);
        String userId = (String) getScenarioContext().getContext(Context.USER_ID);
        RemoveBookRequest rmvBookRequest = new RemoveBookRequest(userId, book.isbn);
        response = getEndPoints().removeBook(rmvBookRequest);
        Assertions.assertEquals(204, response.getStatusCode());
    }
}
