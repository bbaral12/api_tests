package stepDefinitions;

import apiEngine.model.requests.AuthorizationRequest;
import cucumber.TestContext;
import dataProvider.ConfigReader;
import io.cucumber.java.en.Given;

public class AccountSteps extends BaseStep{
    public AccountSteps(TestContext testContext){
        super(testContext);
    }

    @Given("I am a authorized user")
    public void i_am_a_authorized_user() {
        AuthorizationRequest authRequest = new AuthorizationRequest(
                ConfigReader.getInstance().getvalue("userName"),
                ConfigReader.getInstance().getvalue("password"));
        getEndPoints().authenticateUser(authRequest);
    }
}
