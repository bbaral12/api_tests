package stepDefinitions;

import apiEngine.IRestResponse;
import apiEngine.model.Book;
import apiEngine.model.responses.UserAccount;
import cucumber.TestContext;
import enums.Context;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;

public class VerificationSteps extends BaseStep{

    private static IRestResponse<UserAccount> userAccountResponse;

    public VerificationSteps(TestContext testContext){
        super(testContext);
    }

    @Then("The book is added")
    public void the_book_is_added() {
        Book book = (Book) getScenarioContext().getContext(Context.BOOK);
        String userId = (String) getScenarioContext().getContext(Context.USER_ID);
        userAccountResponse = getEndPoints().getUserAccount(userId);
        Assertions.assertEquals(200, userAccountResponse.getStatusCode());
        Assertions.assertEquals(book.isbn, userAccountResponse.getBody().books.get(0).isbn);
    }

    @Then("The book is removed")
    public void the_book_is_removed() {
        String userId = (String) getScenarioContext().getContext(Context.USER_ID);
        userAccountResponse = getEndPoints().getUserAccount(userId);
        Assertions.assertEquals(200, userAccountResponse.getStatusCode());
        Assertions.assertEquals(0, userAccountResponse.getBody().books.size());
    }
}
