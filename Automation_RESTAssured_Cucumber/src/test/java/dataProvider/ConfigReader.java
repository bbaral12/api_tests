package dataProvider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {
    private Properties properties;
    private static ConfigReader configReader;

    private ConfigReader() {
        BufferedReader reader;
        String propertiesFilePath = "configs//configuration.properties";
        try {
            reader = new BufferedReader(new FileReader(propertiesFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration properties not found at:" + propertiesFilePath);
        }
    }

    public static ConfigReader getInstance() {
        if (configReader == null) {
            configReader = new ConfigReader();
        }
        return configReader;
    }

    public String getvalue(String valName) {
        String valueOf = properties.getProperty(valName);
        if (valueOf != null) {
            return valueOf;
        } else {
            throw new RuntimeException(valName + " not specified in the configuration.properties file");
        }
    }
}

