Feature: E2E Test for BookStore API

  Background: User generates token for authorization
    Given I am a authorized user

  Scenario: Authorized user is able to add and remove a book.
    Given A list of books are available
    When I add a book to my reading list
    Then The book is added
    When I remove the book from my reading list
    Then The book is removed